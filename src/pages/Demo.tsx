import { DemoStore } from "../stores";
import { inject, observer } from "mobx-react";
import React from 'react';

interface DemoProps {
  demoStore: DemoStore
}

@inject('demoStore')
@observer
class Demo extends React.Component<DemoProps> {
  public render() {
    return (
      <section className="hero is-link is-fullheight">
        <div className="hero-body has-text-centered">
          <div className="container">
            <h1 className="title">Interview</h1>
            <h2 className="subtitle">{this.props.demoStore.prompt}</h2>
          </div>
        </div>
      </section>
    );
  }
}

export default Demo;