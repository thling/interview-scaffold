import { BrowserRouter, Route } from "react-router-dom";
import React from 'react';
import Demo from "../pages/Demo";

export default () =>
    <BrowserRouter>
      <Route exact path="/" component={Demo} />
    </BrowserRouter>;