import { observable } from "mobx";

export class DemoStore {
  @observable public prompt = 'Hello, my name is Sam.';
}

export const demoStore = new DemoStore();